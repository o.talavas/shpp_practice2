package shpp.level0;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import shpp.level0.number.ExtendedNumber;
import shpp.level0.number.NumberSequence;
import shpp.level0.util.Config;

public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        Config config = new Config();
        String type = System.getProperty("type", "int");

        logger.debug("System property type = {}", type);

        ExtendedNumber min = null;
        ExtendedNumber max = null;
        ExtendedNumber inc = null;


        try{
            min = ExtendedNumber.convertFromString(config.getProperty("min"), type);
            max = ExtendedNumber.convertFromString(config.getProperty("max"), type);
            inc = ExtendedNumber.convertFromString(config.getProperty("inc"), type);
        }catch (NumberFormatException e){
            logger.error("Invalid input values for creating NumberSequence.", e);
        }


        logger.info("Numbers from app.properties converted successfully.");
        logger.debug("min={}, max={}, inc={}", min, max, inc);

        long time = System.currentTimeMillis();
        if(min != null && max != null && inc != null){
            try{
                NumberSequence numberSequence = new NumberSequence(min, max, inc);

                for (ExtendedNumber num1 : numberSequence) {
                    for (ExtendedNumber num2 : numberSequence) {
                        logger.info("{} * {} = {}",num1, num2, num1.multiply(num2));
                    }
                }
                logger.debug("Execution time={} ms", System.currentTimeMillis() - time);
            }catch (NumberFormatException e){
                logger.error("Can't create number sequence.", e);
            }

        }else{
            logger.error("Invalid input values for creating multiply table.");
            logger.info("Check numbers in app.properties file");
        }

    }
}