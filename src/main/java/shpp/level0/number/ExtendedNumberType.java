package shpp.level0.number;

public enum ExtendedNumberType {
    INTEGER("int"),
    SHORT("short"),
    LONG("long"),
    BYTE("byte"),
    DOUBLE("double"),
    FLOAT("float"),
    BIG_INTEGER("biginteger"),
    BIG_DECIMAL("bigdecimal");

    private final String typeName;

    ExtendedNumberType(String typeName) {
        this.typeName = typeName;
    }

    public static ExtendedNumberType fromTypeName(String typeName) {
        for (ExtendedNumberType type : ExtendedNumberType.values()) {
            if (type.typeName.equalsIgnoreCase(typeName)) {
                return type;
            }
        }
        return ExtendedNumberType.INTEGER;
    }
}
