package shpp.level0.number;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.BigInteger;

public class ExtendedNumber {
    private static final Logger logger = LoggerFactory.getLogger(ExtendedNumber.class);
    private final BigInteger bigInteger;
    private final BigDecimal bigDecimal;
    public static final ExtendedNumber ZERO_INTEGER = ExtendedNumber.create(ExtendedNumberType.SHORT, "0");
    public static final ExtendedNumber ZERO_DECIMAL = ExtendedNumber.create(ExtendedNumberType.FLOAT, "0");

    private ExtendedNumber(BigInteger bigInteger) {
        this.bigInteger = bigInteger;
        this.bigDecimal = null;
    }

    private ExtendedNumber(BigDecimal bigDecimal) {
        this.bigInteger = null;
        this.bigDecimal = bigDecimal;
    }

    /**
     * Return zero as ExtendedNumber class object
     * @return zero value
     */
    public ExtendedNumber zero(){
        if(bigInteger != null){
            return ZERO_INTEGER;
        }
        if(bigDecimal != null){
            return ZERO_DECIMAL;
        }
        return null;
    }

    public Number toNumber() {
        if (bigDecimal != null) {
            return bigDecimal;
        }
        return bigInteger;
    }

    public ExtendedNumber multiply(ExtendedNumber that) {
        if (this.bigDecimal != null && that.bigDecimal != null) {
            return new ExtendedNumber(bigDecimal.multiply(that.bigDecimal));
        }
        if (this.bigInteger != null && that.bigInteger != null) {
            return new ExtendedNumber(bigInteger.multiply(that.bigInteger));
        }
        if(this.bigInteger != null && that.bigDecimal != null){
            return new ExtendedNumber(new BigDecimal(this.bigInteger.toString()).multiply(that.bigDecimal));
        }
        if(this.bigDecimal != null && that.bigInteger != null){
            return new ExtendedNumber(this.bigDecimal.multiply(new BigDecimal(that.bigInteger.toString())));
        }
        return null;
    }

    public ExtendedNumber add(ExtendedNumber that) {
        if (bigDecimal != null && that.bigDecimal != null) {
            return new ExtendedNumber(bigDecimal.add(that.bigDecimal));
        }
        if(bigInteger != null && that.bigInteger != null) {
            return new ExtendedNumber(bigInteger.add(that.bigInteger));
        }
        return null;
    }


    public int compareTo(ExtendedNumber that) {
        if (bigDecimal != null && that.bigDecimal != null) {
            return bigDecimal.compareTo(that.bigDecimal);
        }
        if (bigInteger != null && that.bigInteger != null){
            return bigInteger.compareTo(that.bigInteger);
        }
        throw new ArithmeticException("Can't compare number with NULL value");
    }

    public static ExtendedNumber convertFromString(String value, String type) throws NumberFormatException{
        ExtendedNumberType numberType = ExtendedNumberType.fromTypeName(type);
        return create(numberType, value);
    }

    @Override
    public String toString() {
        if(bigInteger != null ){
            return bigInteger.toString();
        }
        if (bigDecimal != null) {
            return bigDecimal.toString();
        }
        return "no value";
    }

    protected static ExtendedNumber create(ExtendedNumberType type, String value) throws NumberFormatException{
       try{
           switch (type) {
               case INTEGER:
                   return new ExtendedNumber(new BigInteger(String.valueOf(Integer.valueOf(value))));
               case SHORT:
                   return new ExtendedNumber(new BigInteger(String.valueOf(Short.valueOf(value))));
               case LONG:
                   return new ExtendedNumber(new BigInteger(String.valueOf(Long.valueOf(value))));
               case BYTE:
                   return new ExtendedNumber(new BigInteger(String.valueOf(Byte.valueOf(value))));
               case BIG_INTEGER:
                   return new ExtendedNumber(new BigInteger(value));
               case DOUBLE:
                   return new ExtendedNumber(new BigDecimal(String.valueOf(Double.valueOf(value))));
               case FLOAT:
                   return new ExtendedNumber(new BigDecimal(String.valueOf(Float.valueOf(value))));
               case BIG_DECIMAL:
                   return new ExtendedNumber(new BigDecimal(value));
           }
       }catch (NumberFormatException e){
           logger.error("Can't convert String={} to Number type={}", value, type, e);
       }
        throw new NumberFormatException("Can't convert String " + value + " to number");
    }
}
