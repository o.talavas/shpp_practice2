package shpp.level0.number;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;

public class NumberSequence implements Iterable<ExtendedNumber> {

    private final Logger logger = LoggerFactory.getLogger(NumberSequence.class);
    private final ExtendedNumber min;
    private final ExtendedNumber max;
    private final ExtendedNumber increment;

    public NumberSequence(ExtendedNumber min, ExtendedNumber max, ExtendedNumber increment) throws NumberFormatException{
        if(max.compareTo(min) <= 0){
            logger.error("Can't create number sequence cause max={} is lower than min={}", max, min);
            throw new NumberFormatException("Max value " + max + " is lower than min value " + min);
        }
        if(increment.compareTo(increment.zero()) <= 0){
            logger.error("Can't create number sequence with zero increment.");
            throw new NumberFormatException("Increment should be > zero.");
        }
        this.min = min;
        this.max = max;
        this.increment = increment;
        logger.debug("Number sequence from min={} to max={} with increment={} created", min, max, increment);
    }

    @Override
    public Iterator<ExtendedNumber> iterator() {
        return new NumberSequenceIterator(min, max, increment);
    }

    private static class NumberSequenceIterator implements Iterator<ExtendedNumber> {
        private ExtendedNumber current;
        private final ExtendedNumber max;
        private final ExtendedNumber increment;

        public NumberSequenceIterator(ExtendedNumber min, ExtendedNumber max, ExtendedNumber increment) {
            this.current = min;
            this.max = max;
            this.increment = increment;
        }

        @Override
        public boolean hasNext() {
            return current.compareTo(max) <= 0;
        }

        @Override
        public ExtendedNumber next() {
            ExtendedNumber result = current;
            current = current.add(increment);

            return result;
        }
    }
}