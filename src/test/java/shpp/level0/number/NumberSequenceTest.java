package shpp.level0.number;

import org.junit.jupiter.api.Test;

import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

class NumberSequenceTest {

    @Test
    void testIncorrectMinMaxIncrement() {
        ExtendedNumber min = ExtendedNumber.create(ExtendedNumberType.INTEGER, "10");
        ExtendedNumber max = ExtendedNumber.create(ExtendedNumberType.INTEGER, "0");
        ExtendedNumber increment = ExtendedNumber.create(ExtendedNumberType.INTEGER, "1");

        assertThrows(IllegalArgumentException.class, () -> new NumberSequence(min, max, increment));

        ExtendedNumber minNegative = ExtendedNumber.create(ExtendedNumberType.INTEGER, "-1");
        ExtendedNumber maxNegative = ExtendedNumber.create(ExtendedNumberType.INTEGER, "-10");
        ExtendedNumber incrementNegative = ExtendedNumber.create(ExtendedNumberType.INTEGER, "-2");

        assertThrows(IllegalArgumentException.class,
                () -> new NumberSequence(minNegative, maxNegative, incrementNegative));

        ExtendedNumber minA = ExtendedNumber.create(ExtendedNumberType.INTEGER, "-10");
        ExtendedNumber  maxA= ExtendedNumber.create(ExtendedNumberType.INTEGER, "-1");
        ExtendedNumber incA = ExtendedNumber.create(ExtendedNumberType.INTEGER, "-2");

        assertThrows(IllegalArgumentException.class,
                () -> new NumberSequence(minA, maxA, incA));
    }


    @Test
    void testIteratorWithInteger() {
        ExtendedNumber min = ExtendedNumber.create(ExtendedNumberType.INTEGER, "0");
        ExtendedNumber max = ExtendedNumber.create(ExtendedNumberType.INTEGER, "10");
        ExtendedNumber increment = ExtendedNumber.create(ExtendedNumberType.INTEGER, "2");

        NumberSequence sequence = new NumberSequence(min, max, increment);

        Iterator<ExtendedNumber> it = sequence.iterator();

        assertTrue(it.hasNext());
        assertEquals(0, it.next().toNumber().intValue());
        assertTrue(it.hasNext());
        assertEquals(2, it.next().toNumber().intValue());
        assertTrue(it.hasNext());
        assertEquals(4, it.next().toNumber().intValue());
        assertTrue(it.hasNext());
        assertEquals(6, it.next().toNumber().intValue());
        assertTrue(it.hasNext());
        assertEquals(8, it.next().toNumber().intValue());
        assertTrue(it.hasNext());
        assertEquals(10, it.next().toNumber().intValue());
        assertFalse(it.hasNext());
    }

    @Test
    void testIteratorWithNegativeInteger() {
        ExtendedNumber min = ExtendedNumber.create(ExtendedNumberType.INTEGER, "-10");
        ExtendedNumber max = ExtendedNumber.create(ExtendedNumberType.INTEGER, "0");
        ExtendedNumber increment = ExtendedNumber.create(ExtendedNumberType.INTEGER, "2");
        NumberSequence sequence = new NumberSequence(min, max, increment);

        Iterator<ExtendedNumber> it = sequence.iterator();

        assertTrue(it.hasNext());
        assertEquals(-10, it.next().toNumber().intValue());
        assertTrue(it.hasNext());
        assertEquals(-8, it.next().toNumber().intValue());
        assertTrue(it.hasNext());
        assertEquals(-6, it.next().toNumber().intValue());
        assertTrue(it.hasNext());
        assertEquals(-4, it.next().toNumber().intValue());
        assertTrue(it.hasNext());
        assertEquals(-2, it.next().toNumber().intValue());
        assertTrue(it.hasNext());
        assertEquals(0, it.next().toNumber().intValue());
        assertFalse(it.hasNext());
    }

    @Test
    void testIteratorWithFloat() {
        ExtendedNumber min = ExtendedNumber.create(ExtendedNumberType.FLOAT, "1.5");
        ExtendedNumber max = ExtendedNumber.create(ExtendedNumberType.FLOAT, "3.5");
        ExtendedNumber increment = ExtendedNumber.create(ExtendedNumberType.FLOAT, "0.5");

        NumberSequence sequence = new NumberSequence(min, max, increment);

        Iterator<ExtendedNumber> it = sequence.iterator();

        assertTrue(it.hasNext());
        assertEquals(1.5f, it.next().toNumber().floatValue(), 0.001f);
        assertTrue(it.hasNext());
        assertEquals(2.0f, it.next().toNumber().floatValue(), 0.001f);
        assertTrue(it.hasNext());
        assertEquals(2.5f, it.next().toNumber().floatValue(), 0.001f);
        assertTrue(it.hasNext());
        assertEquals(3.0f, it.next().toNumber().floatValue(), 0.001f);
        assertTrue(it.hasNext());
        assertEquals(3.5f, it.next().toNumber().floatValue(), 0.001f);
        assertFalse(it.hasNext());
    }

    @Test
    void testIteratorWithNegativeDouble() {
        ExtendedNumber min = ExtendedNumber.create(ExtendedNumberType.DOUBLE, "-3.37");
        ExtendedNumber max = ExtendedNumber.create(ExtendedNumberType.DOUBLE, "1.31");
        ExtendedNumber increment = ExtendedNumber.create(ExtendedNumberType.DOUBLE, "0.57");

        NumberSequence sequence = new NumberSequence(min, max, increment);

        Iterator<ExtendedNumber> it = sequence.iterator();

        assertTrue(it.hasNext());
        assertEquals(-3.37, it.next().toNumber().doubleValue(), 0.001);
        assertTrue(it.hasNext());
        assertEquals(-2.8, it.next().toNumber().doubleValue(), 0.001);
        assertTrue(it.hasNext());
        assertEquals(-2.23, it.next().toNumber().doubleValue(), 0.001);
        assertTrue(it.hasNext());
        assertEquals(-1.66, it.next().toNumber().doubleValue(), 0.001);
        assertTrue(it.hasNext());
        assertEquals(-1.09, it.next().toNumber().doubleValue(), 0.001);
        assertTrue(it.hasNext());
        assertEquals(-0.52, it.next().toNumber().doubleValue(), 0.001);
        assertTrue(it.hasNext());
        assertEquals(0.05, it.next().toNumber().doubleValue(), 0.001);
        assertTrue(it.hasNext());
        assertEquals(0.62, it.next().toNumber().doubleValue(), 0.001);
        assertTrue(it.hasNext());
        assertEquals(1.19, it.next().toNumber().doubleValue(), 0.001);
        assertFalse(it.hasNext());
    }
}
