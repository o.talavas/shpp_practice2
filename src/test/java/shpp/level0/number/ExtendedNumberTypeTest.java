package shpp.level0.number;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExtendedNumberTypeTest {

    @Test
    void fromTypeName() {
        assertEquals(ExtendedNumberType.INTEGER, ExtendedNumberType.fromTypeName("int"));
        assertEquals(ExtendedNumberType.SHORT, ExtendedNumberType.fromTypeName("short"));
        assertEquals(ExtendedNumberType.LONG, ExtendedNumberType.fromTypeName("long"));
        assertEquals(ExtendedNumberType.BYTE, ExtendedNumberType.fromTypeName("byte"));
        assertEquals(ExtendedNumberType.DOUBLE, ExtendedNumberType.fromTypeName("double"));
        assertEquals(ExtendedNumberType.FLOAT, ExtendedNumberType.fromTypeName("float"));
        assertEquals(ExtendedNumberType.BIG_INTEGER, ExtendedNumberType.fromTypeName("biginteger"));
        assertEquals(ExtendedNumberType.BIG_DECIMAL, ExtendedNumberType.fromTypeName("bigdecimal"));
    }

    @Test
    void testFromTypeNameUnknownType() {
        assertEquals(ExtendedNumberType.INTEGER, ExtendedNumberType.fromTypeName("unknown"));
    }

    @Test
    void testFromTypeNameCaseInsensitive() {
        assertEquals(ExtendedNumberType.INTEGER, ExtendedNumberType.fromTypeName("INT"));
        assertEquals(ExtendedNumberType.SHORT, ExtendedNumberType.fromTypeName("Short"));
        assertEquals(ExtendedNumberType.LONG, ExtendedNumberType.fromTypeName("LONG"));
        assertEquals(ExtendedNumberType.BYTE, ExtendedNumberType.fromTypeName("BYTE"));
        assertEquals(ExtendedNumberType.DOUBLE, ExtendedNumberType.fromTypeName("dOuBlE"));
        assertEquals(ExtendedNumberType.FLOAT, ExtendedNumberType.fromTypeName("fLoAt"));
        assertEquals(ExtendedNumberType.BIG_INTEGER, ExtendedNumberType.fromTypeName("BigInteger"));
        assertEquals(ExtendedNumberType.BIG_DECIMAL, ExtendedNumberType.fromTypeName("BigDecimal"));
    }
}