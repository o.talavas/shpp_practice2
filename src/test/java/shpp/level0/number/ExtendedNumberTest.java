package shpp.level0.number;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

class ExtendedNumberTest {

    @Test
    void createExtendedNumberFromIncorrectString(){
        assertThrows(NumberFormatException.class, () -> ExtendedNumber.create(ExtendedNumberType.DOUBLE, "10,0"));
        assertThrows(NumberFormatException.class, () -> ExtendedNumber.create(ExtendedNumberType.SHORT, "999999999999999999999999999999"));
    }

    @Test
    void multiply_twoIntegers() {
        ExtendedNumber a = ExtendedNumber.create(ExtendedNumberType.INTEGER, "10");
        ExtendedNumber b = ExtendedNumber.create(ExtendedNumberType.INTEGER, "5");

        ExtendedNumber result = a.multiply(b);

        assertEquals(Integer.valueOf("50"), result.toNumber().intValue());

        a = ExtendedNumber.create(ExtendedNumberType.INTEGER, "-5");
        b = ExtendedNumber.create(ExtendedNumberType.INTEGER, "6");

        result = a.multiply(b);

        assertEquals(Integer.valueOf("-30"), result.toNumber().intValue());
    }

    @Test
    void multiply_twoIntegerMaxValue(){
        ExtendedNumber a = ExtendedNumber.create(ExtendedNumberType.INTEGER, String.valueOf(Integer.MAX_VALUE));
        BigInteger maxInt = new BigInteger(String.valueOf(Integer.MAX_VALUE));

        ExtendedNumber result = a.multiply(a);
        BigInteger expected = maxInt.multiply(maxInt);

        assertEquals(expected, result.toNumber());
    }

    @Test
    void multiply_twoDoubleMaxValue(){
        ExtendedNumber a = ExtendedNumber.create(ExtendedNumberType.DOUBLE, String.valueOf(Double.MAX_VALUE));
        BigDecimal maxDouble = new BigDecimal(String.valueOf(Double.MAX_VALUE));

        ExtendedNumber result = a.multiply(a);
        BigDecimal expected = maxDouble.multiply(maxDouble);

        assertEquals(expected, result.toNumber());
    }

    @Test
    void multiply_twoFloatMaxValue(){
        ExtendedNumber a = ExtendedNumber.create(ExtendedNumberType.FLOAT, String.valueOf(Float.MAX_VALUE));
        BigDecimal maxFloat = new BigDecimal(String.valueOf(Float.MAX_VALUE));

        ExtendedNumber result = a.multiply(a);
        BigDecimal expected = maxFloat.multiply(maxFloat);

        assertEquals(expected, result.toNumber());
    }

    @Test
    void multiply_twoFloats() {
        ExtendedNumber a = ExtendedNumber.create(ExtendedNumberType.FLOAT, "0.000001");
        ExtendedNumber b = ExtendedNumber.create(ExtendedNumberType.FLOAT, "1000000.0");


        ExtendedNumber result = a.multiply(b);

        assertEquals(Float.valueOf("1.0"), result.toNumber().floatValue());
    }

    @Test
    void multiply_BigIntegerAndFloat() {
        ExtendedNumber a = ExtendedNumber.create(ExtendedNumberType.BIG_INTEGER, "1000");
        ExtendedNumber b = ExtendedNumber.create(ExtendedNumberType.FLOAT, "0.1");

        ExtendedNumber result = a.multiply(b);

        assertEquals(new BigDecimal("100.0"), result.toNumber());
    }

    @Test
    void multiply_FloatAndInteger() {
        ExtendedNumber a = ExtendedNumber.create(ExtendedNumberType.FLOAT, "0.002");
        ExtendedNumber b = ExtendedNumber.create(ExtendedNumberType.INTEGER, "50000");

        ExtendedNumber result = a.multiply(b);

        assertEquals(Float.valueOf("100.0"), result.toNumber().floatValue());
    }

    @Test
    void multiply_byZero() {
        ExtendedNumber a = ExtendedNumber.create(ExtendedNumberType.INTEGER, "10");
        ExtendedNumber b = ExtendedNumber.create(ExtendedNumberType.INTEGER, "0");

        ExtendedNumber result = a.multiply(b);

        assertEquals(Integer.valueOf("0"), result.toNumber().intValue());
    }

    @Test
    void multiply_largeIntegers() {
        BigInteger bigA = new BigInteger("123456789012345678901234567890");
        BigInteger bigB = new BigInteger("987654321098765432109876543210");
        ExtendedNumber a = ExtendedNumber.create(ExtendedNumberType.BIG_INTEGER, bigA.toString());
        ExtendedNumber b = ExtendedNumber.create(ExtendedNumberType.BIG_INTEGER, bigB.toString());

        ExtendedNumber result = a.multiply(b);

        BigInteger expected = bigA.multiply(bigB);
        assertEquals(expected, result.toNumber());
    }

    @Test
    void multiply_largeDecimals() {
        BigDecimal bigA = new BigDecimal("12345678901234567890.1234567890");
        BigDecimal bigB = new BigDecimal("98765432109876543210.9876543210");
        ExtendedNumber a = ExtendedNumber.create(ExtendedNumberType.BIG_DECIMAL, bigA.toString());
        ExtendedNumber b = ExtendedNumber.create(ExtendedNumberType.BIG_DECIMAL, bigB.toString());

        ExtendedNumber result = a.multiply(b);

        BigDecimal expected = bigA.multiply(bigB);
        assertEquals(expected, result.toNumber());
    }

    @Test
    void testAdd() {
        ExtendedNumber integer1 = ExtendedNumber.create(ExtendedNumberType.INTEGER, "2147483647"); // Integer.MAX_VALUE
        ExtendedNumber integer2 = ExtendedNumber.create(ExtendedNumberType.INTEGER, "1");
        Assertions.assertEquals("2147483648", integer1.add(integer2).toString());

        ExtendedNumber double1 = ExtendedNumber.create(ExtendedNumberType.DOUBLE, String.valueOf(Double.MAX_VALUE));
        ExtendedNumber double2 = ExtendedNumber.create(ExtendedNumberType.DOUBLE, String.valueOf(Double.MAX_VALUE));
        BigDecimal bigDouble = new BigDecimal(Double.MAX_VALUE);
        Assertions.assertEquals(bigDouble.add(bigDouble).doubleValue(), double1.add(double2).toNumber().doubleValue(), 0.0001f);

        ExtendedNumber long1 = ExtendedNumber.create(ExtendedNumberType.LONG, "9223372036854775807");
        ExtendedNumber long2 = ExtendedNumber.create(ExtendedNumberType.LONG, "1");
        Assertions.assertEquals("9223372036854775808", long1.add(long2).toString());

        ExtendedNumber float1 = ExtendedNumber.create(ExtendedNumberType.FLOAT, String.valueOf(Float.MAX_VALUE));
        ExtendedNumber float2 = ExtendedNumber.create(ExtendedNumberType.FLOAT, String.valueOf(Float.MAX_VALUE));
        BigDecimal bigFloat = new BigDecimal(Float.MAX_VALUE);
        Assertions.assertEquals(bigFloat.add(bigFloat).floatValue(), float1.add(float2).toNumber().floatValue());

        ExtendedNumber byte1 = ExtendedNumber.create(ExtendedNumberType.BYTE, String.valueOf(Byte.MAX_VALUE));
        ExtendedNumber byte2 = ExtendedNumber.create(ExtendedNumberType.BYTE, "1");
        Assertions.assertEquals(String.valueOf(Byte.MAX_VALUE + 1), byte1.add(byte2).toString());
    }
}